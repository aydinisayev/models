/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author aydin
 */
@Entity
@Table(name = "persons1")
@NamedQueries({
    @NamedQuery(name = "Persons1.findAll", query = "SELECT p FROM Persons1 p")})
public class Persons1 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Column(name = "birthdate")
    @Temporal(TemporalType.DATE)
    private Date birthdate;
    @Size(max = 255)
    @Column(name = "gender")
    private String gender;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Size(max = 255)
    @Column(name = "patronymic")
    private String patronymic;
    @Size(max = 255)
    @Column(name = "place_of_birth")
    private String placeOfBirth;
    @Size(max = 255)
    @Column(name = "place_of_residence")
    private String placeOfResidence;
    @Size(max = 255)
    @Column(name = "surname")
    private String surname;
    @JoinColumn(name = "partiya_id", referencedColumnName = "id")
    @ManyToOne
    private Partiya1 partiyaId;
    @JoinColumn(name = "workplace_id", referencedColumnName = "personid")
    @ManyToOne
    private WorkPlace1 workplaceId;

    public Persons1() {
    }

    public Persons1(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getPlaceOfResidence() {
        return placeOfResidence;
    }

    public void setPlaceOfResidence(String placeOfResidence) {
        this.placeOfResidence = placeOfResidence;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Partiya1 getPartiyaId() {
        return partiyaId;
    }

    public void setPartiyaId(Partiya1 partiyaId) {
        this.partiyaId = partiyaId;
    }

    public WorkPlace1 getWorkplaceId() {
        return workplaceId;
    }

    public void setWorkplaceId(WorkPlace1 workplaceId) {
        this.workplaceId = workplaceId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persons1)) {
            return false;
        }
        Persons1 other = (Persons1) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Persons1[ id=" + id + " ]";
    }
    
}
