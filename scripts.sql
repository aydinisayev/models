CREATE TABLE `persons1` (
  `id` bigint(20) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `patronymic` varchar(255) DEFAULT NULL,
  `place_of_birth` varchar(255) DEFAULT NULL,
  `place_of_residence` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `partiya_id` bigint(20) DEFAULT NULL,
  `workplace_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_partiya_id1` (`partiya_id`),
  KEY `fk_workplace_id1` (`workplace_id`),
  CONSTRAINT `fk_partiya_id1` FOREIGN KEY (`partiya_id`) REFERENCES `partiya1` (`id`),
  CONSTRAINT `fk_workplace_id1` FOREIGN KEY (`workplace_id`) REFERENCES `work_place1` (`personid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



CREATE TABLE `partiya1` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `partiyaname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `work_place1` (
  `personid` bigint(20) NOT NULL,
  `end_date` date DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `work_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`personid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

